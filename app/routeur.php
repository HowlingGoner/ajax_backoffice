<?php

/*
 * ./app/routeur.php
 * ROUTEUR PRINCIPAL
 * But :
 *      Charger le controleur et lancer l'action
 *      qui correspond à ce qui est demandé
 *      par l'internaute
 */

use Controleurs\Membres;

if (isset($_GET['membres'])):
  include_once '../app/routeurs/membresRouteur.php';
  
else:
  include_once '../app/controleurs/membresControleur.php';
  Membres\indexAction($connexion, 1);
endif;
