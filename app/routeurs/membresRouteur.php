<?php

/*
    ./app/routeurs/membresRouteur.php
 */

  include_once '../app/controleurs/membresControleur.php';
  use \Controleurs\Membres;

  switch($_GET['membres']):
    case 'add':
      Membres\addAction($connexion, [
        'nom'    => $_POST['nom'],
        'prenom' => $_POST['prenom'],
        'tel'    => $_POST['tel'],
        'email'  => $_POST['email']
      ]);
      break;
    case 'delete':
      Membres\deleteAction($connexion, $_GET['id']);
      break;
    case 'edit':
      Membres\editAction($connexion, [
        'id'     => $_GET['id'],
        'nom'    => $_POST['nom'],
        'prenom' => $_POST['prenom'],
        'tel'    => $_POST['tel'],
        'email'  => $_POST['email']
      ]);
      break;
  endswitch;
