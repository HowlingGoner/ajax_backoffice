<?php

/*
 * ./app/vues/membres/details.php
 * Variables disponibles
 *      - $membre : ARRAY(id, nom, prenom, tel, email, actif)
 */
?>
<tr data-id="<?php echo $membre['id']; ?>">
    <td><div class="nom"><?php echo $membre['nom']; ?></div></td>
    <td><div class="prenom"><?php echo $membre['prenom']; ?></div></td>
    <td><div class="tel"><?php echo $membre['tel']; ?></div></td>
    <td><div class="email"><?php echo $membre['email']; ?></div></td>
    <td>
        <div>
            <span><a class="delete red-text" href="#"><i class="material-icons left">indeterminate_check_box</i></a><span>
            <span><a class="edit" href="#"><i class="material-icons">settings</i></a></span>
        </div>
    </td>
</tr>
