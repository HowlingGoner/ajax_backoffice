<?php

/*
 * ./app/vues/membres/index.php
 * Variables disponibles
 *      - $membres : ARRAY(ARRAY(id, nom, prenom, tel, email, actif))
 */
?>
<h1>Membres du personnel</h1>

<div id="message" class="card-panel teal lighten-2 hide"></div>

<table class="bordered striped highlight responsive-table" id="tableGestion">
    <thead>
        <tr class="">
            <th class="input-field">
                <input type="text" id="nom" required="required" />
                <label for="nom">Nom</label>
            </th>
            <th class="input-field">
                <input type="text" id="prenom" required="required" />
                <label for="prenom">Prénom</label>
            </th>
            <th class="input-field">
                <input type="tel" id="tel" required="required" />
                <label for="tel">Tel</label>
            </th>
            <th class="input-field">
                <input type="email" id="email" required="required" />
                <label for="email">Email</label>
            </th>
            <th>
                <a id="add" href="#"><i class="material-icons left green-text">add_box</i></a>
            </th>
        </tr>
    </thead>
    <tbody>
         <!-- Début de région à répéter -->
            <?php foreach($membres as $membre): ?>
              <?php include '../app/vues/membres/details.php'; ?>
            <?php endforeach; ?>
            <!-- Fin de région à répéter -->
    </tbody>
</table>
