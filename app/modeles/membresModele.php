<?php

/*
 * ./app/modeles/membresModele.php
 * Modèle pour les membres
 */

namespace Modeles\Membre;


      /**
       * [findAll description]
       * @param  PDO    $connexion [description]
       * @return [type]            [description]
       */
        function findAll(\PDO $connexion){
           $sql = "SELECT * "
                   . "FROM membres "
                   . "ORDER BY nom ASC, prenom ASC;";
           $membres = $connexion->query($sql);
           return $membres->fetchAll(\PDO::FETCH_ASSOC);
        }

    /**
     * [findOneById description]
     * @param  PDO    $connexion [description]
     * @param  int    $id        [description]
     * @return [type]            [description]
     */
        function findOneById(\PDO $connexion, int $id){
           $sql = "SELECT * "
                   . "FROM membres "
                   . "WHERE id = :id;";
           $rs = $connexion->prepare($sql);
           $rs->bindValue(':id', $id, \PDO::PARAM_INT);
           $rs->execute();
           return $rs->fetch(\PDO::FETCH_ASSOC);
        }

        function insert(\PDO $connexion, array $data = null){
          $sql = "INSERT INTO membres
                  SET nom        = :nom,
                      prenom     = :prenom,
                      tel        = :tel,
                      email      = :email,
                      created_at = NOW();";
          $rs = $connexion->prepare($sql);
          $rs->bindValue(':nom', $data['nom'], \PDO::PARAM_STR);
          $rs->bindValue(':prenom', $data['prenom'], \PDO::PARAM_STR);
          $rs->bindValue(':tel', $data['tel'], \PDO::PARAM_STR);
          $rs->bindValue(':email', $data['email'], \PDO::PARAM_STR);
          $rs->execute();
          return $connexion->lastInsertId();
        }

        function deleteOneById(\PDO $connexion, int $id){
          $sql = "DELETE FROM membres
                  WHERE id = :id;";
          $rs = $connexion->prepare($sql);
          $rs->bindValue(':id', $id, \PDO::PARAM_INT);
          return intval($rs->execute());
        }

        function updateOneById(\PDO $connexion, array $data = null){
          $sql = "UPDATE membres
                  SET nom        = :nom,
                      prenom     = :prenom,
                      tel        = :tel,
                      email      = :email
                  WHERE id = :id;";
          $rs = $connexion->prepare($sql);
          $rs->bindValue(':nom', $data['nom'], \PDO::PARAM_STR);
          $rs->bindValue(':prenom', $data['prenom'], \PDO::PARAM_STR);
          $rs->bindValue(':tel', $data['tel'], \PDO::PARAM_STR);
          $rs->bindValue(':email', $data['email'], \PDO::PARAM_STR);
          $rs->bindValue(':id', $data['id'], \PDO::PARAM_INT);
          return intval($rs->execute());
        }
